import { render, screen } from '@testing-library/react';
import App from './App';

test('renders app', () => {
  render(<App />);
  const linkElement = screen.getByText(/할일 관리/i);
  expect(linkElement).toBeInTheDocument();
});

