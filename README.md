# GitLab CI/CD Hands-on

### Todo 앱을 10분안에 CI/CD!

React 기반의 web을 GitLab CI/CD를 사용하여 GKE 환경에 배포합니다.


## 시작하기

[핸즈온 공간](https://gitlab.com/groups/GitLab-KR/gcp_handson/cicd/)를 활용하여 Hands on을 진행합니다.

- 깃랩 계정이 없으면 가입하기
- 핸즈온 공간에 Request Access 하기

Hands on의 시작부터 종료까지 모든 과정이 담겨있습니다.

[핸즈온 슬라이드](https://bit.ly/ig-cicd-handson)

[Hands on 실습코드](https://bubbly-bottle-ef9.notion.site/GitLab-Hands-on-82fb107ea18f487abfd00a90c576144a)

슬라이드와 실습코드는 Hands on 진행 기간에만 오픈되며 Hands on이 종료되면 닫힙니다.




